# Git Befehle für den "daily-use"



## Repository das erste Mal herunterladen

 `git clone <url des repo>`

## Repository aktualisieren vor dem Arbeiten

 `git pull`

## Geänderte Files für den Commit "markieren"

 `git add <filename1>`

`git add <filename2>`

oder einfach alle Files im Repo:

 `git add .`

## Files commiten

 `git commit -m "<commit message>"`

Es können lokal auch mehrere Commits gemacht werden, bevor diese wieder auf den Server "geschoben" werden.

## Alle commits auf den Server schieben

 `git push`